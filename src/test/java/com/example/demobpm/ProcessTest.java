package com.example.demobpm;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.CaseService;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricVariableInstance;
import org.camunda.bpm.engine.runtime.CaseExecution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProcessTest {

  @Autowired
  private RuntimeService runtimeService;

  @Autowired
  private TaskService taskService;

  @Autowired
  private CaseService caseService;

  @Autowired
  private HistoryService historyService;

  private void printVariables(String pid) {
    List<HistoricVariableInstance> variables = historyService.createHistoricVariableInstanceQuery().processInstanceId(pid).list();
    variables.stream().forEach(
        hvi -> {
          log.info("{} => {}", hvi.getName(), hvi.getValue());
        }
    );
  }

  @Test
  public void testProcessApproveWithoutVerification() {
    String pid;
    { // start process instance
      Map<String,Object> data = new HashMap<>();
      data.put("provideDocsStatus", "");
      data.put("status", "");
      ProcessInstance pi = runtimeService.startProcessInstanceByKey("ApproveProcess", data);
      pid = pi.getId();
    }

    { // complete approval task
      List<Task> tasks = taskService.createTaskQuery().taskDefinitionKey("ApprovalTask").list();
      Map<String,Object> data = new HashMap<>();
      data.put("status", "approved");
      String taskId = tasks.get(0).getId();
      taskService.complete(taskId, data);
    }

    { // process should be ended
      long count = runtimeService.createProcessInstanceQuery().processInstanceId(pid).count();
      Assert.assertEquals(0, count);
    }

    printVariables(pid);
  }

  @Test
  public void testProcessRejectWithoutVerification() {
    String pid;
    { // start process instance
      Map<String,Object> data = new HashMap<>();
      data.put("provideDocsStatus", "");
      data.put("status", "");
      ProcessInstance pi = runtimeService.startProcessInstanceByKey("ApproveProcess", data);
      pid = pi.getId();
    }

    { // complete approval task
      List<Task> tasks = taskService.createTaskQuery().taskDefinitionKey("ApprovalTask").list();
      Map<String,Object> data = new HashMap<>();
      data.put("status", "rejected");
      String taskId = tasks.get(0).getId();
      taskService.complete(taskId, data);
    }

    { // process should be ended
      long count = runtimeService.createProcessInstanceQuery().processInstanceId(pid).count();
      Assert.assertEquals(0, count);
    }

    printVariables(pid);
  }

  @Test
  public void testProcessCancelVerification() {
    String pid;
    { // start process instance
      Map<String,Object> data = new HashMap<>();
      data.put("provideDocsStatus", "");
      data.put("status", "");
      ProcessInstance pi = runtimeService.startProcessInstanceByKey("ApproveProcess", data);
      pid = pi.getId();
    }

    { // check tasks
      // provide docs task should be
      long count = taskService.createTaskQuery().taskDefinitionKey("ProvideDocsTask").count();
      Assert.assertEquals(1, count);
    }

    { // cancel verification
      List<Task> tasks = taskService.createTaskQuery().taskDefinitionKey("CancelVerification").list();
      String taskId = tasks.get(0).getId();
      taskService.complete(taskId);
    }

    { // check tasks
      // no provide docs task
      long count = taskService.createTaskQuery().taskDefinitionKey("ProvideDocsTask").count();
      Assert.assertEquals(0, count);
    }

    { // complete approval task
      List<Task> tasks = taskService.createTaskQuery().taskDefinitionKey("ApprovalTask").list();
      Map<String,Object> data = new HashMap<>();
      data.put("status", "rejected");
      String taskId = tasks.get(0).getId();
      taskService.complete(taskId, data);
    }

    { // process should be ended
      long count = runtimeService.createProcessInstanceQuery().processInstanceId(pid).count();
      Assert.assertEquals(0, count);
    }

    printVariables(pid);
  }

}
